run: 
	@air -c .air.toml

setup-dev: 
	@docker-compose up

clean-dev:
	@docker-compose down

migrate:
	@go build -o ./tmp/migrate ./cmd/migrate && ./tmp/migrate
	
test:
	@godotenv -f ./.env go test ./pkg/app/server/...