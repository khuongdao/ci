package qoute

import (
	"catalog/pkg/domain"
)

func (uc *UseCase) Create(quote domain.Quote) (string, error) {
	//var dto dtos.QuoteInfoDTO
	id, err := uc.Qoute.Create(quote)
	if err != nil {
		return "", err
	}

	return id, nil
}
