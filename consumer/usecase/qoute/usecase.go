package qoute

import (
	"catalog/consumer/repository/qoute"
)

type UseCase struct {
	Qoute qoute.Repository
}

func New(repo qoute.Repository) *UseCase {
	return &UseCase{
		Qoute: repo,
	}
}
