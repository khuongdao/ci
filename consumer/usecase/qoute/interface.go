package qoute

import (
	"catalog/pkg/domain"
	"catalog/pkg/dtos"
)

type IUseCase interface {
	//Create(ctx context.Context, req *CreateRequest) (*ResponseWrapper, error)
	//Update(ctx context.Context, req *UpdateRequest) (*ResponseWrapper, error)
	GetByID(id string) (*dtos.QuoteInfoDTO, error)
	Create(qoute domain.Quote) (string, error)
	//GetList(ctx context.Context, req *GetListRequest) (*ListResponseWrapper, error)
	//Delete(ctx context.Context, req *DeleteRequest) error
}

type ResponseWrapper struct {
	Qoute *domain.Quote `json:"example"`
}

type ListResponseWrapper struct {
	Qoutes []domain.Quote `json:"examples"`
	Meta   interface{}    `json:"meta"`
}
