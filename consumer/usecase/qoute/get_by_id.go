package qoute

import (
	"catalog/pkg/dtos"
)

func (uc *UseCase) GetByID(id string) (*dtos.QuoteInfoDTO, error) {
	//var dto dtos.QuoteInfoDTO
	resp, err := uc.Qoute.GetByID(id)
	if err != nil {
		return nil, err
	}

	return resp, nil
}
