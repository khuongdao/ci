package qoute

import (
	"catalog/pkg/domain"
	"catalog/pkg/dtos"
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func NewPG(client *mongo.Client) Repository {
	return pgRepository{client}
}

type pgRepository struct {
	*mongo.Client
}

func (client pgRepository) Create(quote domain.Quote) (string, error) {
	_, err := client.Database("quote_db").Collection("Examples").InsertOne(context.TODO(), quote)

	if err != nil {
		return "", err
	}
	return quote.ID, nil
}

func (client pgRepository) GetByID(id string) (*dtos.QuoteInfoDTO, error) {
	var dto dtos.QuoteInfoDTO
	//objId, _ := primitive.ObjectIDFromHex(id)

	coll := client.Database("quote_db").Collection("Examples")
	err := coll.FindOne(context.TODO(), bson.D{{"quote_id", id}}).Decode(&dto)

	if err != nil {
		return nil, err
	}

	//cursor, err := coll.Find(context.TODO(), bson.M{})
	//if err != nil {
	//	return nil, err
	//}
	//var episodes []bson.M
	//if err = cursor.All(context.TODO(), &episodes); err != nil {
	//	return nil, err
	//}
	//fmt.Println(episodes)

	return &dto, nil
}
