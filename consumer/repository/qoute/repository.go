package qoute

import (
	"catalog/pkg/domain"
	"catalog/pkg/dtos"
)

type Repository interface {
	GetByID(id string) (*dtos.QuoteInfoDTO, error)
	Create(quote domain.Quote) (string, error)
}
