package quote

import (
	"catalog/pkg/domain"
	"catalog/pkg/handlers"
	"github.com/labstack/echo/v4"
	"net/http"
)

func (r *Route) create(c echo.Context) error {
	var quote domain.Quote
	err := c.Bind(&quote)
	if err != nil {
		return handlers.HTTPRes(c, http.StatusOK, err.Error(), nil)
	}

	resp, err := r.useCase.Qoute.Create(quote)
	if err != nil {
		return handlers.HTTPRes(c, http.StatusOK, err.Error(), nil)
	}

	return handlers.HTTPRes(c, http.StatusOK, "Get quotes Success", map[string]string{"id": resp})
}
