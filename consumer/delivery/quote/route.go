package quote

import (
	usecase "catalog/consumer/usecase/qoute"
	"github.com/labstack/echo/v4"
)

type Route struct {
	useCase *usecase.UseCase
}

func Init(group *echo.Group, useCase *usecase.UseCase) {
	r := &Route{useCase}

	//group.POST("", r.create)
	//group.GET("", r.getList)
	group.GET("/:id", r.getByID)
	//group.PUT("/:id", r.update)
	//group.DELETE("/:id", r.delete)
}
