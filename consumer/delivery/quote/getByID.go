package quote

import (
	"catalog/pkg/handlers"
	"github.com/labstack/echo/v4"
	"net/http"
)

func (r *Route) getByID(c echo.Context) error {
	var (
		idStr = c.Param("id")
	)
	
	resp, err := r.useCase.Qoute.GetByID(idStr)
	if err != nil {
		return handlers.HTTPRes(c, http.StatusOK, err.Error(), nil)
	}

	return handlers.HTTPRes(c, http.StatusOK, "Get quotes Success", resp)
}
