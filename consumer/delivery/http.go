package http

import (
	"catalog/consumer/delivery/quote"
	usecase "catalog/consumer/usecase/qoute"
	"github.com/labstack/echo/v4"
)

func NewHTTPHandler(useCase *usecase.UseCase) *echo.Echo {
	var (
		e = echo.New()
	)

	// APIs
	api := e.Group("/api")
	quote.Init(api.Group("/qoute"), useCase)

	return e
}
