package main

import (
	"catalog/cmd/rabbitmq"
	repository "catalog/consumer/repository/qoute"
	"catalog/consumer/usecase/qoute"
	"catalog/pkg/config"
	"catalog/pkg/database"
	"catalog/pkg/domain"
	"catalog/pkg/services"
	"encoding/json"
	"fmt"
	"log"
)

func rabbitmqRun(cfg *config.AppConfig, usecase *qoute.UseCase) {
	url := fmt.Sprintf("amqp://%s:%s@localhost:%d", cfg.AMQP.USERNAME, cfg.AMQP.PASSWORD, cfg.AMQP.PORT)
	mq, err := rabbitmq.NewMQ(url)
	if err != nil {
		panic(err)
	}
	defer mq.Close()

	_, err = mq.QueueDeclare(rabbitmq.NewQueueOptions().SetName("INSERT"))
	if err != nil {
		panic(err)
	}

	log.Println(" [*] Waiting for messages. To exit press CTRL+C")

	results, err := mq.Consume(nil)
	if err != nil {
		panic(err)
	}

	for result := range results {
		var data services.Rabbitmqmessage
		err := json.Unmarshal(result.Body, &data)
		if err != nil {
			panic(err)
		}
		usecase.Qoute.Create(domain.Quote{
			ID:      data.ID,
			Content: data.Content,
			UserId:  data.UserID,
		})
		log.Println("insert success!")
		result.Ack(false)
	}
}

func main() {
	cfg, err := config.NewConfig()
	if err != nil {
		panic(err)
	}
	client, err := database.NewConnectionMongo(cfg.MONGO.URI)
	if err != nil {
		panic(err)
	}
	repo := repository.NewPG(client)
	usecase := qoute.New(repo)

	rabbitmqRun(cfg, usecase)
	//forever := make(chan bool)
	//<-forever
}
