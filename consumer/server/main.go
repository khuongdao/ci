package main

import (
	"catalog/consumer/delivery/quote"
	repository "catalog/consumer/repository/qoute"
	"catalog/consumer/usecase/qoute"
	"catalog/pkg/config"
	"catalog/pkg/database"
	"github.com/labstack/echo/v4"
)

func main() {
	e := echo.New()
	cfg, err := config.NewConfig()
	if err != nil {
		panic(err)
	}
	client, err := database.NewConnectionMongo(cfg.MONGO.URI)
	if err != nil {
		panic(err)
	}
	repo := repository.NewPG(client)
	usecase := qoute.New(repo)

	api := e.Group("/api")
	quote.Init(api.Group("/qoute"), usecase)
	//http
	e.Start(":1234")

	//forever := make(chan bool)
	//<-forever
}
