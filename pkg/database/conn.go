package database

import (
	"context"
	"fmt"
	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
	"time"
)

func NewConnection(uri string) (*gorm.DB, error) {
	db, err := gorm.Open(mysql.Open(uri), &gorm.Config{})
	log.Println(uri)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return db, nil
}

func NewConnectionMongo(uri string) (*mongo.Client, error) {

	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)

	defer cancel()

	// Set client options
	clientOptions := options.Client().ApplyURI(uri) // use env variables
	// Connect to MongoDB
	mongoDB, err := mongo.Connect(ctx, clientOptions)

	databases, _ := mongoDB.ListDatabaseNames(ctx, bson.M{})

	if err != nil {
		return nil, err
	}

	fmt.Println(databases)
	fmt.Println("Connected to MongoDB successfully!")

	return mongoDB, err
}
