package handlers

import (
	"catalog/pkg/dtos"
	"catalog/pkg/pagination"
	srv "catalog/pkg/services"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
)

type QuoteHandler struct {
	srv srv.IQuoteService
}

func NewQuoteHandler(s srv.IQuoteService) *QuoteHandler {
	return &QuoteHandler{srv: s}
}

func (h *QuoteHandler) InsertQuote(c echo.Context) error {
	dto := new(dtos.QuoteInsertDTO)

	if err := c.Bind(dto); err != nil {
		return HTTPRes(c, http.StatusBadRequest, err.Error(), nil)
	}

	userId := c.Get("user_id").(string)
	// fmt.Println(id)

	userId64, err := strconv.ParseUint(userId, 10, 32)

	if err != nil {
		return err
	}

	dto.UserId = uint(userId64)

	id, err := h.srv.InsertQuote(dto)

	if err != nil {
		return HTTPRes(c, http.StatusOK, err.Error(), nil)
	}

	return HTTPRes(c, http.StatusOK, "Create quote successfully", map[string]string{"id": id})
}

func (h *QuoteHandler) GetQuotes(c echo.Context) error {
	var paginate pagination.Pagination

	id := c.Get("user_id").(string)
	// fmt.Println(id)

	userId64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		return HTTPRes(c, http.StatusUnauthorized, err.Error(), nil)
	}

	limit := c.Get("limit").(int)
	page := c.Get("page").(int)

	//set paginate
	paginate.Limit = int(limit)
	paginate.Page = int(page)

	metaData, err := h.srv.GetQuotes(uint(userId64), paginate)
	if err != nil {
		return HTTPRes(c, http.StatusUnauthorized, err.Error(), nil)
	}

	return HTTPRes(c, http.StatusOK, "Get quotes Success", metaData)
}

func (h *QuoteHandler) GetDetail(c echo.Context) error {
	id := c.Param("id")
	userId := c.Get("user_id").(string)

	userId64, err := strconv.ParseUint(userId, 10, 32)
	if err != nil {
		return HTTPRes(c, http.StatusUnauthorized, err.Error(), nil)
	}

	quote, err := h.srv.GetDetail(id, uint(userId64))

	if err != nil {
		return HTTPRes(c, http.StatusUnauthorized, err.Error(), nil)
	}

	return HTTPRes(c, http.StatusOK, "Get quotes Success", quote)
}
