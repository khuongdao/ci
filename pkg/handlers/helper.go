package handlers

import "github.com/labstack/echo/v4"

type Response struct {
	Status int         `json:"status"`
	Msg    string      `json:"msg"`
	Data   interface{} `json:"data"`
}

func HTTPRes(c echo.Context, httpCode int, msg string, data interface{}) error {
	return c.JSON(httpCode, Response{
		Status: httpCode,
		Msg:    msg,
		Data:   data,
	})
}
