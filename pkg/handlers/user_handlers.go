package handlers

import (
	"catalog/pkg/config"
	"catalog/pkg/dtos"
	srv "catalog/pkg/services"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
)

type UserHandler struct {
	srv srv.IUserService
	cfg *config.AppConfig
}

func NewUserHandler(s srv.IUserService, c *config.AppConfig) *UserHandler {
	return &UserHandler{srv: s, cfg: c}
}

func (h *UserHandler) Get(c echo.Context) error {
	return c.JSON(http.StatusOK, map[string]string{"message": "OK!"})
}

func (h *UserHandler) SignUp(c echo.Context) error {
	user := new(dtos.UserSignUpDTO)

	if err := c.Bind(user); err != nil {
		return HTTPRes(c, http.StatusBadRequest, err.Error(), nil)
	}

	stringToken, err := h.srv.SignUp(h.cfg, user)
	if err != nil {

		return HTTPRes(c, http.StatusUnauthorized, err.Error(), nil)
	}

	return HTTPRes(c, http.StatusOK, "Sign up Success", map[string]string{"Token": stringToken})
}

func (h *UserHandler) SignIn(c echo.Context) error {
	user := new(dtos.UserSignInDTO)

	if err := c.Bind(user); err != nil {
		return HTTPRes(c, http.StatusBadRequest, err.Error(), nil)
	}

	stringToken, err := h.srv.SignIn(h.cfg, user)
	if err != nil {

		return HTTPRes(c, http.StatusUnauthorized, err.Error(), nil)
	}

	return HTTPRes(c, http.StatusOK, "Sign in Success", map[string]string{"Token": stringToken})
}

func (h *UserHandler) GetProfile(c echo.Context) error {

	id := c.Get("user_id").(string)
	// fmt.Println(id)

	userId64, err := strconv.ParseUint(id, 10, 32)

	if err != nil {
		return HTTPRes(c, http.StatusUnauthorized, err.Error(), nil)
	}

	user, err := h.srv.GetProfile(uint(userId64))

	if err != nil {
		return HTTPRes(c, http.StatusUnauthorized, err.Error(), nil)
	}

	return HTTPRes(c, http.StatusOK, "Get Profile Success", user)
}
