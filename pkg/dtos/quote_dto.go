package dtos

type QuoteInsertDTO struct {
	Content string `validate:"required" json:"content" form:"content" query:"content"`
	UserId  uint   `json:"userid" form:"userid" query:"userid"`
}

type QuoteInfoDTO struct {
	ID      string `json:"id" form:"id" query:"id" bson:"quote_id"`
	Content string `validate:"required" json:"content" form:"content" query:"content" bson:"content"`
}

type QuotesDTO struct {
	Quotes     []QuoteInfoDTO `json:"quotes,omitempty"`
	Pagination PaginationDTO  `json:"meta"`
}
