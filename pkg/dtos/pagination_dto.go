package dtos

type PaginationDTO struct {
	CurrentPage int `json:"current_page"`
	NextPage    int `json:"next_page"`
	PrevPage    int `json:"prev_page"`
	TotalPages  int `json:"total_pages"`
	TotalItems  int `json:"total_items"`
}
