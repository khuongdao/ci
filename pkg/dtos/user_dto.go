package dtos

type UserSignUpDTO struct {
	UserName string `validate:"required" json:"username" form:"username" query:"username"`
	Email    string `validate:"required" json:"email" form:"email" query:"email"`
	Password string `validate:"required" json:"password" form:"password" query:"password"`
}

type UserSignInDTO struct {
	Email    string `validate:"required" json:"email" form:"email" query:"email"`
	Password string `validate:"required" json:"password" form:"password" query:"password"`
}

type UserProfileDTO struct {
	UserName string `validate:"required" json:"username" form:"username" query:"username"`
	Email    string `validate:"required" json:"email" form:"email" query:"email"`
}
