package tokens

import (
	"errors"
	"strings"
	"time"

	"github.com/golang-jwt/jwt/v4"
)

func CreateToken(secretKey string, userId uint) (string, error) {

	var err error
	//Creating Access Token
	//this should be in an env file
	atClaims := jwt.MapClaims{}
	atClaims["authorized"] = true
	atClaims["user_id"] = userId
	atClaims["exp"] = time.Now().Add(time.Minute * 15).Unix()
	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	token, err := at.SignedString([]byte(secretKey))

	if err != nil {
		return "", err
	}
	return token, nil
}

func ParseToken(stringToken string) (jwt.MapClaims, error) {
	tokenSplit := strings.Split(stringToken, "Bearer ")

	if len(tokenSplit) <= 1 {
		return nil, errors.New("wrong token")
	}

	token, err := jwt.Parse(tokenSplit[1], nil)

	if token == nil {
		return nil, err
	}
	claims, _ := token.Claims.(jwt.MapClaims)

	return claims, nil
}
