package domain

import (
	"catalog/pkg/dtos"
	"time"

	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	UserName string `gorm:"column:username"`
	Email    string `gorm:"email"`
	Password string `gorm:"password"`
}

func (user *User) ToUser(dto *dtos.UserSignUpDTO) {
	user.UserName = dto.UserName
	user.Email = dto.Email
	user.Password = dto.Password
	user.CreatedAt = time.Now()
}
