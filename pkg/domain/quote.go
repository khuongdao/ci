package domain

import (
	"catalog/pkg/dtos"
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Quote struct {
	gorm.Model
	ID      string `gorm:"id" bson:"quote_id"`
	Content string `gorm:"content" bson:"content"`
	UserId  uint   `gorm:"column:userid" bson:"user_id"`
}

func (quote *Quote) ToQuote(dto *dtos.QuoteInsertDTO) {
	quote.ID = uuid.New().String()
	quote.Content = dto.Content
	quote.UserId = dto.UserId
	quote.CreatedAt = time.Now()
}
