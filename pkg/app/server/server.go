package server

import (
	"catalog/pkg/config"
	"catalog/pkg/database"
	"catalog/pkg/handlers"
	"catalog/pkg/middlewares"
	"catalog/pkg/repositories"
	"catalog/pkg/services"
	"log"

	"github.com/labstack/echo/v4"
)

func CreateServer(cfg *config.AppConfig) *echo.Echo {
	app := echo.New()

	db, err := database.NewConnection(cfg.Database.URI)
	if err != nil {
		log.Fatal(err)
	}

	repo := repositories.NewUserRepo(db)
	srv := services.NewUserService(repo)
	userHandler := handlers.NewUserHandler(srv, cfg)

	quoteRepo := repositories.NewQuoteRepo(db)
	quoteSrv := services.NewQuoteService(quoteRepo)
	quoteHandler := handlers.NewQuoteHandler(quoteSrv)

	app.GET("/healthz", HealthCheck)
	app.POST("/sign-up", userHandler.SignUp)
	app.POST("/sign-in", userHandler.SignIn)
	app.GET("/profile", userHandler.GetProfile, middlewares.AuthorizationToken)

	//quote
	app.POST("/quotes", quoteHandler.InsertQuote, middlewares.AuthorizationToken)
	app.GET("/quotes", quoteHandler.GetQuotes, middlewares.AuthorizationToken, middlewares.ProcessLimitAndPage)
	app.GET("/quotes/:id", quoteHandler.GetDetail, middlewares.AuthorizationToken)
	app.GET("/", userHandler.Get)

	return app
}
