package server_test

import (
	"catalog/pkg/app/server"
	"catalog/pkg/config"
	"catalog/pkg/tokens"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gavv/httpexpect/v2"
)

func TestGetAll(t *testing.T) {
	conf, _ := config.NewConfig()
	// app := server.CreateServer(conf)
	s := httptest.NewServer(server.CreateServer(conf))
	defer s.Close()

	e := httpexpect.New(t, s.URL)
	token, _ := tokens.CreateToken(conf.JWT.KEY, 2)

	//var dto = new(dtos.QuotesDTO)

	//dto.Quotes = []dtos.QuoteInfoDTO{{ID: "0e98d17f-97ba-4a9e-b14b-41affb4cc316", Content: "hello world 2"}, {ID: "221d2339-34a3-4d5d-b4d1-14a509e99f3b", Content: "hello world 3"}}
	//dto.Quotes = nil
	//dto.Pagination.TotalItems = 4
	//dto.Pagination.CurrentPage = 1
	//dto.Pagination.TotalPages = 2
	//dto.Pagination.NextPage = 2
	//dto.Pagination.PrevPage = 1
	e.GET("/quotes").WithQueryString("page=1&limit=2").WithHeader("Authorization", "Bearer "+token).
		Expect().
		Status(http.StatusUnauthorized).
		JSON().
		Object().
		ValueEqual("data", nil)
}

func TestGetDetail(t *testing.T) {
	conf, _ := config.NewConfig()
	// app := server.CreateServer(conf)
	s := httptest.NewServer(server.CreateServer(conf))
	defer s.Close()

	e := httpexpect.New(t, s.URL)
	token, _ := tokens.CreateToken(conf.JWT.KEY, 2)

	e.GET("/quotes/221d2339-34a3-4d5d-b4d1-14a509e99f3b").WithHeader("Authorization", "Bearer "+token).
		Expect().
		Status(http.StatusUnauthorized).
		JSON().
		Object().
		ValueEqual("data", nil)
	//dtos.QuoteInfoDTO{ID: "221d2339-34a3-4d5d-b4d1-14a509e99f3b", Content: "hello world 3"}
}
