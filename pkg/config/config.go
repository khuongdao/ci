package config

import (
	"log"

	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
	"github.com/pkg/errors"
)

// AppConfig struct for structuring the config data
type AppConfig struct {
	Database struct {
		URI  string `envconfig:"DB_URL"`
		Name string `envconfig:"DB_NAME"`
	}

	Server struct {
		Port int `envconfig:"PORT"`
	}

	JWT struct {
		KEY string `envconfig:"JWT_KEY"`
	}

	MONGO struct {
		URI string `envconfig:"MONGO_URI"`
	}

	AMQP struct {
		PORT     int    `envconfig:"AMQP_PORT"`
		USERNAME string `envconfig:"AMQP_USERNAME"`
		PASSWORD string `envconfig:"AMQP_PASSWORD"`
	}
}

// NewConfig  a function to read the configuration file and return config struct
func NewConfig() (*AppConfig, error) {
	if err := godotenv.Load(); err != nil {
		log.Println("load env file: ", err)
	}

	cfg := AppConfig{}
	err := envconfig.Process("", &cfg)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return &cfg, nil
}
