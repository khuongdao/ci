package services

import (
	"catalog/cmd/rabbitmq"
	"catalog/pkg/config"
	"catalog/pkg/dtos"
	"encoding/json"
	"fmt"
	"github.com/streadway/amqp"
	"log"
	"sync"
)

type IRabbitmq interface {
	Publisher(id string) error
}

type Rabbitmqmessage struct {
	ID      string `json:"quote_id" bson:"quote_id"`
	Content string `json:"content" bson:"content"`
	UserID  uint   `json:"user_id" bson:"user_id"`
}

func (srv *QuoteService) Publisher(id string, dto *dtos.QuoteInsertDTO) error {
	cfg, err := config.NewConfig()
	if err != nil {
		log.Fatal(err)
	}

	url := fmt.Sprintf("amqp://%s:%s@localhost:%d", cfg.AMQP.USERNAME, cfg.AMQP.PASSWORD, cfg.AMQP.PORT)
	mq, err := rabbitmq.NewMQ(url)

	_, err = mq.QueueDeclare(rabbitmq.NewQueueOptions().SetName("INSERT"))
	if err != nil {
		return err
	}

	data, err := json.Marshal(Rabbitmqmessage{ID: id, Content: dto.Content, UserID: dto.UserId})
	if err != nil {
		return err
	}

	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		defer wg.Done()
		err = mq.Publish(&rabbitmq.MQConfigPublish{
			RoutingKey: mq.Queue().Name,
			Message: amqp.Publishing{
				ContentType: "text/plain",
				Body:        data,
			},
		})
	}()

	wg.Wait()

	return nil
}
