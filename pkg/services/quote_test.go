package services_test

import (
	"catalog/pkg/repositories"
	"catalog/pkg/services"
	"testing"
)

func TestGetDetail(t *testing.T) {
	srv := new(services.QuoteService)
	repo := repositories.NewQuoteMockRepo()
	srv.Repo = repo

	data, err := srv.GetDetail("1", 123)
	if err != nil {
		t.Error(err)
	}
	// log.Println(data)
	if data.ID != "1" || data.Content != "abc" {
		t.Error("Error")
	}
}
