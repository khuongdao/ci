package services

import (
	"catalog/pkg/config"
	"catalog/pkg/domain"
	"catalog/pkg/dtos"
	"catalog/pkg/password"
	"catalog/pkg/tokens"
	"errors"

	"github.com/go-playground/validator/v10"
)

type IUserRepo interface {
	Save(user *domain.User) (uint, error)
	Get(id uint) (*domain.User, error)
	FindUserByEmail(email string) (*domain.User, error)
}

type IUserService interface {
	SignUp(cfg *config.AppConfig, dto *dtos.UserSignUpDTO) (string, error)
	SignIn(cfg *config.AppConfig, dto *dtos.UserSignInDTO) (string, error)
	GetProfile(id uint) (*dtos.UserProfileDTO, error)
}
type UserService struct {
	Repo IUserRepo
}

func NewUserService(repo IUserRepo) IUserService {
	return &UserService{repo}
}

func (srv *UserService) SignUp(cfg *config.AppConfig, dto *dtos.UserSignUpDTO) (string, error) {
	var user = new(domain.User)
	validate := validator.New()
	err := validate.Struct(dto)

	if err != nil {
		return "", err
	}

	u, err := srv.Repo.FindUserByEmail(dto.Email)

	if err != nil {
		return "", err
	}

	if u != nil {

		return "", errors.New("account does exist")
	}

	hashedPassword, err := password.HashPassword(dto.Password)

	if err != nil {
		return "", err
	}

	dto.Password = hashedPassword

	user.ToUser(dto)

	id, err := srv.Repo.Save(user)

	if err != nil {
		return "", err
	}

	return tokens.CreateToken(cfg.JWT.KEY, id)
}

func (srv *UserService) SignIn(cfg *config.AppConfig, dto *dtos.UserSignInDTO) (string, error) {
	validate := validator.New()
	err := validate.Struct(dto)

	if err != nil {
		return "", err
	}

	user, err := srv.Repo.FindUserByEmail(dto.Email)

	if err != nil {
		return "", err
	}

	if user == nil {
		return "", errors.New("wrong email")
	}

	err = password.CompareHashedPassword(user.Password, dto.Password)

	if err != nil {
		return "", err
	}

	return tokens.CreateToken(cfg.JWT.KEY, user.ID)
}

func (srv *UserService) GetProfile(id uint) (*dtos.UserProfileDTO, error) {
	dto := new(dtos.UserProfileDTO)

	user, err := srv.Repo.Get(uint(id))

	if err != nil {
		return nil, err
	}

	dto.UserName = user.UserName
	dto.Email = user.Email

	return dto, nil
}
