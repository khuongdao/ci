package services

import (
	"catalog/pkg/domain"
	"catalog/pkg/dtos"
	"catalog/pkg/pagination"
	"errors"
	"log"
)

type IQuoteRepo interface {
	Save(quote *domain.Quote) (string, error)
	GetAll(id uint, option pagination.Pagination) ([]domain.Quote, *pagination.Pagination, error)
	FindById(id string, userId uint) (*domain.Quote, error)
}

type IQuoteService interface {
	InsertQuote(dto *dtos.QuoteInsertDTO) (string, error)
	GetQuotes(id uint, option pagination.Pagination) (*dtos.QuotesDTO, error)
	GetDetail(id string, userId uint) (*dtos.QuoteInfoDTO, error)
}

type QuoteService struct {
	Repo IQuoteRepo
	Rab  IRabbitmq
}

func NewQuoteService(repo IQuoteRepo) IQuoteService {
	return &QuoteService{Repo: repo}
}

func Offset(p *pagination.Pagination) int {
	return (p.Page - 1) * p.Limit
}

func (srv *QuoteService) InsertQuote(dto *dtos.QuoteInsertDTO) (string, error) {
	quote := new(domain.Quote)

	quote.ToQuote(dto)

	id, err := srv.Repo.Save(quote)

	if err != nil {
		return "", nil
	}

	err = srv.Publisher(id, dto)
	if err != nil {
		return "", nil
	}

	return id, nil
}

func (srv *QuoteService) GetQuotes(id uint, option pagination.Pagination) (*dtos.QuotesDTO, error) {
	var metaData = new(dtos.QuotesDTO)
	quotes, pagination, err := srv.Repo.GetAll(id, option)

	if err != nil {
		return nil, err
	}

	quoteDto := make([]dtos.QuoteInfoDTO, len(quotes))
	var paginate dtos.PaginationDTO

	for idx, quote := range quotes {
		quoteDto[idx].ID = quote.ID
		quoteDto[idx].Content = quote.Content
	}

	paginate.CurrentPage = pagination.Page
	paginate.TotalItems = pagination.TotalItems
	paginate.TotalPages = pagination.TotalPages

	if int(pagination.Page) < paginate.TotalPages {
		paginate.NextPage = (int(pagination.Page) + 1)
	} else {
		paginate.NextPage = paginate.TotalPages
	}

	if int(pagination.Page) > 1 {
		paginate.PrevPage = (int(pagination.Page) - 1)
	} else {
		paginate.PrevPage = 1
	}

	metaData.Quotes = quoteDto
	metaData.Pagination = paginate

	// return srv.Repo.GetAll(pagination)
	return metaData, nil
}

func (srv *QuoteService) GetDetail(id string, userId uint) (*dtos.QuoteInfoDTO, error) {
	log.Println("hello")
	var dto = new(dtos.QuoteInfoDTO)

	quote, err := srv.Repo.FindById(id, userId)
	if err != nil {
		return nil, err
	}
	if quote.ID == "" {
		return nil, errors.New("quote does not exist")
	}

	dto.ID = quote.ID
	dto.Content = quote.Content

	return dto, nil
}
