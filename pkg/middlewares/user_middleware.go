package middlewares

import (
	"catalog/pkg/handlers"
	"catalog/pkg/tokens"
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
)

func AuthorizationToken(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		token := c.Request().Header.Get("Authorization")

		if token == "" {
			return handlers.HTTPRes(c, http.StatusUnauthorized, "Token does not exist", nil)
		}

		claims, err := tokens.ParseToken(token)

		if err != nil {
			return handlers.HTTPRes(c, http.StatusUnauthorized, err.Error(), nil)
		}

		c.Set("user_id", fmt.Sprint(claims["user_id"]))

		return next(c)
	}
}
