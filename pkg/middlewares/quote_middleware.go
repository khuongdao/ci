package middlewares

import (
	"catalog/pkg/handlers"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
)

func ProcessLimitAndPage(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		//get data

		limit := c.QueryParam("limit")
		page := c.QueryParam("page")

		if limit == "" || page == "" {
			return handlers.HTTPRes(c, http.StatusUnauthorized, "limit or page short", nil)
		}

		//parse int
		limit64, err := strconv.ParseUint(limit, 10, 32)
		if err != nil {
			return handlers.HTTPRes(c, http.StatusUnauthorized, err.Error(), nil)
		}

		page64, err := strconv.ParseUint(page, 10, 32)
		if err != nil {
			return handlers.HTTPRes(c, http.StatusUnauthorized, err.Error(), nil)
		}

		if int(limit64) <= 0 || int(page64) <= 0 {
			return handlers.HTTPRes(c, http.StatusUnauthorized, "wrong limit or page", nil)
		}

		c.Set("limit", int(limit64))
		c.Set("page", int(page64))

		return next(c)
	}
}
