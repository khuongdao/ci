package repositories

import (
	"catalog/pkg/domain"
	"catalog/pkg/pagination"
	srv "catalog/pkg/services"
	"errors"
	"math"

	"gorm.io/gorm"
)

type QuoteRepo struct {
	db *gorm.DB
}

func NewQuoteRepo(db *gorm.DB) srv.IQuoteRepo {
	return &QuoteRepo{db}
}

func (repo *QuoteRepo) Save(quote *domain.Quote) (string, error) {
	err := repo.db.Table("quotes").Create(quote).Error

	if err != nil {
		return "", err
	}
	return quote.ID, nil
}

func (repo *QuoteRepo) FindById(id string, userId uint) (*domain.Quote, error) {
	var quote *domain.Quote

	err := repo.db.Table("quotes").Where("id = ?", id).Where("userid = ?", userId).Find(&quote).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, err
	}
	if err == gorm.ErrRecordNotFound {
		return nil, errors.New("not found detail's quote")
	}

	return quote, nil
}

func (repo *QuoteRepo) GetAll(id uint, option pagination.Pagination) ([]domain.Quote, *pagination.Pagination, error) {
	var pagination = new(pagination.Pagination)
	var quotes []domain.Quote
	var totalItems int64
	repo.db.Table("quotes").Where("userid = ?", id).Count(&totalItems)
	if totalItems == 0 {
		pagination.Page = 0
	} else {
		pagination.Page = option.Page
	}
	pagination.Limit = option.Limit
	pagination.TotalItems = int(totalItems)

	totalPages := int(math.Ceil(float64(totalItems) / float64(pagination.Limit)))
	pagination.TotalPages = totalPages
	err := repo.db.Where("userid = ?", id).Offset(srv.Offset(pagination)).Limit(pagination.Limit).Find(&quotes).Error

	if err != nil {
		return nil, nil, err
	}

	return quotes, pagination, nil
}
