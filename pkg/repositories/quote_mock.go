package repositories

import (
	"catalog/pkg/domain"
	"catalog/pkg/pagination"
	srv "catalog/pkg/services"
)

type QuoteRepoTest struct {
}

func NewQuoteMockRepo() srv.IQuoteRepo {
	return &QuoteRepoTest{}
}

func (repo *QuoteRepoTest) Save(quote *domain.Quote) (string, error) {

	return "", nil
}

func (repo *QuoteRepoTest) FindById(id string, userId uint) (*domain.Quote, error) {
	var data = domain.Quote{
		ID:      id,
		Content: "abc",
		UserId:  userId,
	}
	return &data, nil
}

func (repo *QuoteRepoTest) GetAll(id uint, option pagination.Pagination) ([]domain.Quote, *pagination.Pagination, error) {

	return nil, nil, nil
}
