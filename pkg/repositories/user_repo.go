package repositories

import (
	"catalog/pkg/domain"
	srv "catalog/pkg/services"

	"gorm.io/gorm"
)

type UserRepo struct {
	db *gorm.DB
}

func NewUserRepo(db *gorm.DB) srv.IUserRepo {
	return &UserRepo{db}
}

func (repo *UserRepo) FindUserByEmail(email string) (*domain.User, error) {
	var user domain.User
	err := repo.db.Table("users").Where("email = ?", email).First(&user).Error

	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, err
	}

	if err == gorm.ErrRecordNotFound {
		return nil, nil
	}

	return &user, nil
}

func (repo *UserRepo) Save(user *domain.User) (uint, error) {

	err := repo.db.Model(user).Create(user).Error

	if err != nil {
		return 0, err
	}

	return user.ID, nil
}

func (repo *UserRepo) Get(id uint) (*domain.User, error) {
	user := new(domain.User)
	err := repo.db.Table("users").Where("id = ?", id).First(user).Error

	if err != nil {
		return nil, err
	}

	return user, nil
}
