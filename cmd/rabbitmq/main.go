package rabbitmq

import (
	"catalog/pkg/config"
	"fmt"
	"github.com/streadway/amqp"
	"log"
)

func ConnectRabbitmq() (*amqp.Connection, error) {
	cfg, err := config.NewConfig()
	if err != nil {
		log.Fatal(err)
	}

	url := fmt.Sprintf("amqp://%s:%s@localhost:%d", cfg.AMQP.USERNAME, cfg.AMQP.PASSWORD, cfg.AMQP.PORT)
	conn, err := amqp.Dial(url)
	if err != nil {
		return nil, err
	}

	return conn, nil

	// Let's start by opening a channel to our RabbitMQ instance
	// over the connection we have already established

}
