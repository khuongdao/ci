package main

import (
	"catalog/pkg/app/server"
	"catalog/pkg/config"
	"fmt"
	"log"
)

func main() {
	conf, err := config.NewConfig()

	if err != nil {
		log.Fatal(err)
	}

	app := server.CreateServer(conf)

	addr := fmt.Sprintf(":%d", conf.Server.Port)
	if err := app.Start(addr); err != nil {
		app.Logger.Fatal(err)
	}

}
