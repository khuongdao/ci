package main

import (
	"catalog/pkg/config"
	"catalog/pkg/database"
	"log"

	_ "github.com/go-sql-driver/mysql"
	"github.com/golang-migrate/migrate"
	"github.com/golang-migrate/migrate/database/mysql"
	_ "github.com/golang-migrate/migrate/source/file"
)

func main() {
	cfg, err := config.NewConfig()
	if err != nil {
		log.Fatal(err)
	}

	db, err := database.NewConnection(cfg.Database.URI)
	if err != nil {
		log.Fatal(err)
	}

	dbInstance, err := db.DB()
	if err != nil {
		log.Fatal(err)
	}

	driver, err := mysql.WithInstance(dbInstance, &mysql.Config{})

	folderMigration := "file://migrations"
	m, err := migrate.NewWithDatabaseInstance(folderMigration, cfg.Database.Name, driver)
	if err != nil {
		log.Fatal(err)
	}

	if err := m.Up(); err != nil {
		log.Fatal("migration has failed: ", err)
	}

	log.Println("Migration Done!")
}
